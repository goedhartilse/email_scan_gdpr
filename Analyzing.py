#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 10:38:51 2018

@author: ilsegoedhart
"""

import pandas as pd
import string
import nltk
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import TfidfVectorizer

## analyzing dataset
#
#matrix_A = pd.read_hdf('matrix_A.hdf' , 'mydata')
#matrix_B = pd.read_hdf('matrix_B.hdf' , 'mydata')
#
## merge two matrixes
#labels = pd.concat([matrix_A, matrix_B])
#labels = labels.reset_index()
#labels = labels.drop(['index'], axis = 1)
#
#labels.to_hdf('labels.hdf', 'mydata', mode = 'w')

labels = pd.read_hdf('labels.hdf', 'mydata')

# add column which represents personal data or not
labels = labels.fillna(value = 0)
labels['Personal_data'] = labels['Age'] + labels['Bonus'] + labels['Company'] + labels['Education'] + labels['Email'] + labels['Fax'] + labels['Job_title'] + labels['Phone_number'] + labels['Salary']
labels.loc[labels['Personal_data'] >= 1, 'Personal_data'] = 1

# Analyzing data
print(labels.shape)

print(labels.columns)

print(labels.info())

print(labels.sum())

test = labels.sum()
test = test.drop(['bodies', 'path_data'])
plot = test.plot.bar()
fig = plot.get_figure()
fig.savefig("barplot.png")
# in het plaatje vallen de x-as titels weg, hoe kan ik dit fixen?

# heb dit van internet, weet niet helemaal of het klopt?
df_corr = labels.corr()['Personal_data'][:-1] # -1 because the latest row is SalePrice
golden_features_list = df_corr[abs(df_corr) > 0.5].sort_values(ascending=False)
print("There is {} strongly correlated values with SalePrice:\n{}".format(len(golden_features_list), golden_features_list))



## labels df mergen met de woorden matrix
#path_data = labels['path_data'].tolist()
#
#
## Empty list for all email bodies
#email_bodies = []
#
#for path in path_data:
#    print(path)
#    email = open(path, "r").read()
#    
#    # Delete all information as; from, to, subject, date, time, etc.
#    content_email = email.split("X-FileName:")
#    email_body = ""
#    if len(content_email) > 1:
#        
#        # Remove punctuations
#        text_string = content_email[1].translate(str.maketrans("", "", string.punctuation))
#        
#        # Split up sesntence into single words
#        single_words = nltk.word_tokenize(text_string)
#        
#        # Create English stemmer
#        stemmer = SnowballStemmer("english")
#        
#        # Empty list for all stems
#        stems = []
#        
#        # Extract stem for each word
#        for word in single_words:
#            stem = stemmer.stem(word)
#            stems.append(stem)
#        
#        # Attach all words into a string which is the body of the email
#        email_body = ' '.join(stem for stem in stems)
#        
#        # Fill empty email_bodies list with all email bodies
#        email_bodies.append(email_body)
#
## Create tf idf values and remove English stopwords
#vectorizer = TfidfVectorizer(stop_words = "english", lowercase=True)
#matrix = vectorizer.fit_transform(email_bodies)
#
## Return all words of matrix
#vocab_list = vectorizer.get_feature_names()
#
## Create panda datframe
#matrixdf = pd.DataFrame(matrix.toarray())
#matrixdf["path_data"] = path_data
#matrixdf = matrixdf.set_index("path_data")
#matrixdf.columns = vocab_list
#matrixdf.info()
#
#matrixdf.to_hdf('tdidfmatrix.hdf', 'mydata', mode='w')

tdidfmatrix = pd.read_hdf('tdidfmatrix.hdf', 'mydata')
result = pd.merge(tdidfmatrix, labels, how = 'inner', on=['path_data'])



# deze code komt van https://towardsdatascience.com/random-forest-in-python-24d0893d51c0
import numpy as np
# Labels are the values we want to predict
y = np.array(result['Personal_data'])
# Remove the labels from the features
# axis 1 refers to the columns
features = result.drop(['Age', 'Bonus', 'Company', 'Education', 'Email', 'Fax', 'Job_title', 'Name', 'Phone_number', 'Salary', 'path_data', 'bodies', 'Personal_data'], axis = 1)
# Saving feature names for later use
feature_list = list(features.columns)
# Convert to numpy array
features = np.array(features)

# Using Skicit-learn to split data into training and testing sets
from sklearn.model_selection import train_test_split
# Split the data into training and testing sets
train_features, test_features, train_y, test_y = train_test_split(features, y, test_size = 0.25, random_state = 42)

print('Training Features Shape:', train_features.shape)
print('Training Labels Shape:', train_y.shape)
print('Testing Features Shape:', test_features.shape)
print('Testing Labels Shape:', test_y.shape)

# Import the model we are using
from sklearn.ensemble import RandomForestRegressor
# Instantiate model with 1000 decision trees
rf = RandomForestRegressor(n_estimators = 2, random_state = 42)
# Train the model on training data
rf.fit(train_features, train_y)

predictions = rf.predict(test_features)
# Calculate the absolute errors
errors = abs(predictions - test_y)
# Print out the mean absolute error (mae)
print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')

# Get numerical feature importances
importances = list(rf.feature_importances_)
# List of tuples with variable and importance
feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(feature_list, importances)]
# Sort the feature importances by most important first
feature_importances = sorted(feature_importances, key = lambda x: x[1], reverse = True)
# Print out the feature and importances 
[print('Variable: {:20} Importance: {}'.format(*pair)) for pair in feature_importances];

# New random forest with only the two most important variables
rf_most_important = RandomForestRegressor(n_estimators= 1000, random_state=42)
# Extract the two most important features
important_indices = [feature_list.index('messag'), feature_list.index('manag')]
train_important = train_features[:, important_indices]
test_important = test_features[:, important_indices]
# Train the random forest
rf_most_important.fit(train_important, train_labels)
# Make predictions and determine the error
predictions = rf_most_important.predict(test_important)
errors = abs(predictions - test_labels)
# Display the performance metrics
print('Mean Absolute Error:', round(np.mean(errors), 2), 'degrees.')
mape = np.mean(100 * (errors / test_labels))
accuracy = 100 - mape
print('Accuracy:', round(accuracy, 2), '%.')
