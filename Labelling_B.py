#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 15 14:48:17 2018

@author: ilsegoedhart
"""

import os
import string
import nltk
import pandas as pd
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import TfidfVectorizer

## Trying to get the matrix into a pd dataframe
## import dask.dataframe as dd
## from scipy.sparse import csr_matrix
#
## Print pathnames and safe
#rootdir = "../maildir/"
#path_data = []
#
#for directory, subdirectory, files in os.walk(rootdir):
#    for name in files:
#        path = os.path.join(directory, name)
#        path_data.append(path)
#
## Delete paths with DS_Store, because it does not exist
#path_matrix = pd.DataFrame({'path_data': path_data})
#path_matrix = path_matrix[~path_matrix.path_data.str.contains('DS_Store')]
#
## Keep only emails from the sent folder, to prevent double email context in matrix
#path_matrix = path_matrix[path_matrix.path_data.str.contains('/sent/')]
#
## Keep only last 28166 emails
#path_matrix = path_matrix.tail(28166)
#
## Change pd to list
#path_data = path_matrix['path_data'].tolist()
#
## Empty list for all email bodies
#email_bodies = []
#
#for path in path_data:
#    print(path)
#    email = open(path, "r").read()
#    
#    # Delete all information as; from, to, subject, date, time, etc.
#    content_email = email.split("X-FileName:")
#    email_body = ""
#    if len(content_email) > 1:
#        
#        # Keep body of email
#        text_string = content_email[1]
#        
#        email_bodies.append(text_string)
#
## Search in pd dataframe for: salary, bonus
#body_matrix = pd.DataFrame(email_bodies)
#body_matrix["path_data"] = path_data
#body_matrix = body_matrix.set_index("path_data")
#body_matrix = body_matrix.rename(columns={0: 'bodies'})
#
#body_matrix.to_hdf('bodymatrixB.hdf', 'mydata', mode = 'w')

body_matrix_B = pd.read_hdf('bodymatrixB.hdf' , 'mydata')

salary_matrix_B = body_matrix_B[body_matrix_B.bodies.str.contains('salary')]
salary_matrix_B = salary_matrix_B.reset_index()
salary_matrix_B['Name'] = 1
salary_matrix_B.loc[[2,4,5,6,10,11,12,13,15], 'Name'] = 0

salary_matrix_B['Email'] = 0
salary_matrix_B.loc[[1,3,8,9,12,17,21], 'Email'] = 1

salary_matrix_B['Job_title'] = 0
salary_matrix_B.loc[[3,7,17], 'Job_title'] = 1

salary_matrix_B['Phone_number'] = 0
salary_matrix_B.loc[[3,17], 'Phone_number'] = 1




bonus_matrix_B = body_matrix_B[body_matrix_B.bodies.str.contains('bonus')]
bonus_matrix_B = bonus_matrix_B.reset_index()
bonus_matrix_B['Name'] = 1
bonus_matrix_B.loc[[9,20,22,25,26,33,39,40,51], 'Name'] = 0

bonus_matrix_B['Email'] = 0
bonus_matrix_B.loc[[0,1,2,3,4,6,14,16,18,23,24,27,36,38,42,43,45,46,49,50,52,54], 'Email'] = 1

bonus_matrix_B['Job_title'] = 0
bonus_matrix_B.loc[[3,4,6,23,42,43,45,48,49], 'Job_title'] = 1

bonus_matrix_B['Phone_number'] = 0
bonus_matrix_B.loc[[6,27,29,35,36,38,48,53], 'Phone_number'] = 1

bonus_matrix_B['Bonus'] = 0
bonus_matrix_B.loc[[15,48], 'Bonus'] = 1

bonus_matrix_B['Age'] = 0
bonus_matrix_B.loc[[49,50,52], 'Age'] = 1





# Vergelijk de twee matrixen op dubbelen en dan de dubbelen verwijderen
matrix_B = pd.concat([bonus_matrix_B, salary_matrix_B])
matrix_B = matrix_B.reset_index()
matrix_B.loc[[68,70], 'Name'] = 1
matrix_B.loc[[70], 'Phone_number'] = 1
matrix_B.loc[[70], 'Email'] = 1
matrix_B.loc[[76], 'Job_title'] = 1
matrix_B.loc[[76], 'Age'] = 1
matrix_B = matrix_B.drop_duplicates('path_data')
matrix_B = matrix_B.drop(['index'], axis = 1)


## Synonyms salary
salaries_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('salaries')]
salaries_matrix = salaries_matrix.reset_index()
salaries_matrix = salaries_matrix[~salaries_matrix.path_data.isin(matrix_B.path_data)]

salaries_matrix['Name'] = 1
salaries_matrix.loc[[6],'Name'] = 0

salaries_matrix['Email'] = 0
salaries_matrix.loc[[1,2],'Email'] = 1

# merge met matrix_B
matrix_B = pd.concat([matrix_B, salaries_matrix])
matrix_B = matrix_B.reset_index()
matrix_B = matrix_B.drop(['index'], axis = 1)




wage_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('wage')]
wage_matrix = wage_matrix.reset_index()
wage_matrix = wage_matrix[~wage_matrix.path_data.isin(matrix_B.path_data)]
wage_matrix = wage_matrix.reset_index()
wage_matrix = wage_matrix.drop(['index'], axis = 1)

wage_matrix['Name'] = 1
wage_matrix.loc[[6,7], 'Name'] = 0

wage_matrix['Email'] = 0
wage_matrix.loc[[3,4,5,8,9,11,12,16], 'Email'] = 1

wage_matrix['Job_title'] = 0
wage_matrix.loc[[12,13], 'Job_title'] = 1

wage_matrix['Phone_number'] = 0
wage_matrix.loc[[12], 'Phone_number'] = 1

# merge with matrix_B
matrix_B = pd.concat([matrix_B, wage_matrix])
matrix_B = matrix_B.reset_index()
matrix_B = matrix_B.drop(['index'], axis = 1)




pay_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('pay')]
# zijn er heel veel dus doe ik later






income_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('income')]
income_matrix = income_matrix.reset_index()
income_matrix = income_matrix[~income_matrix.path_data.isin(matrix_B.path_data)]
income_matrix = income_matrix.reset_index()
income_matrix = income_matrix.drop(['index'], axis = 1)

income_matrix['Name'] = 1
income_matrix.loc[[8,30,32,36,46,47,56,66,87,97,98], 'Name'] = 0

income_matrix['Phone_number'] = 0
income_matrix.loc[[1,3,6,7,12,15,20,22,29,31,49,50,60,64,78,81,86,91,92], 'Phone_number'] = 1

income_matrix['Fax'] = 0
income_matrix.loc[[1,3,6,7,12,20,22,29,31,60,64,78,91], 'Fax'] = 1

income_matrix['Email'] = 0
income_matrix.loc[[1,3,4,6,7,14,18,20,22,26,28,29,31,34,37,55,57,59,64,78,81,82,84,86,91,92,93,94,95,96,99,100,101], 'Email'] = 1

income_matrix['Job_title'] = 0
income_matrix.loc[[1,3,12,13,14,19,21,27,51,64,70,73,81,86,88,89,90,91,92], 'Job_title'] = 1

income_matrix['Age'] = 0
income_matrix.loc[[94,95,96,99,100], 'Age'] = 1

# merge with matrix_B
matrix_B = pd.concat([matrix_B, income_matrix])
matrix_B = matrix_B.reset_index()
matrix_B = matrix_B.drop(['index'], axis = 1)




payment_matrix_B = body_matrix_B[body_matrix_B.bodies.str.contains('payment')]
# zijn er heel veel, doe ik later of niet




allowance_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('allowance')]
allowance_matrix = allowance_matrix.reset_index()
allowance_matrix = allowance_matrix[~allowance_matrix.path_data.isin(matrix_B.path_data)]
allowance_matrix = allowance_matrix.reset_index()
allowance_matrix = allowance_matrix.drop(['index'], axis = 1)

allowance_matrix['Name'] = 1
allowance_matrix.loc[[3,15,17,19,20], 'Name'] = 0

allowance_matrix['Email'] = 0
allowance_matrix.loc[[0,10,11,12], 'Email'] = 1

allowance_matrix['Job_title'] = 0
allowance_matrix.loc[[12,18], 'Job_title'] = 1

allowance_matrix['Phone_number'] = 0
allowance_matrix.loc[[12,18], 'Phone_number'] = 1

allowance_matrix['Fax'] = 0
allowance_matrix.loc[[12], 'Fax'] = 1

# merge with matrix_B
matrix_B = pd.concat([matrix_B, allowance_matrix])
matrix_B = matrix_B.reset_index()
matrix_B = matrix_B.drop(['index'], axis = 1)




fee_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('fee')]
# zijn er wel heel veel



wages_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('wages')]
# zijn allemaal al gelabelled


compensation_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('compensation')]
compensation_matrix = compensation_matrix.reset_index()
compensation_matrix = compensation_matrix[~compensation_matrix.path_data.isin(matrix_B.path_data)]
compensation_matrix = compensation_matrix.reset_index()
compensation_matrix = compensation_matrix.drop(['index'], axis = 1)

compensation_matrix['Name'] = 1
compensation_matrix.loc[[14,23,26,30], 'Name'] = 0

compensation_matrix['Job_title'] = 0
compensation_matrix.loc[[2,15,19,20,25], 'Job_title'] = 1

compensation_matrix['Email'] = 0
compensation_matrix.loc[[2,4,5,6,7,8,9,15,21], 'Email'] = 1

compensation_matrix['Phone_number'] = 0
compensation_matrix.loc[[8,9,15,22], 'Phone_number'] = 1

compensation_matrix['Fax'] = 0
compensation_matrix.loc[[8,9], 'Fax'] = 1

# merge to matrix_B
matrix_B = pd.concat([matrix_B, compensation_matrix])
matrix_B = matrix_B.reset_index()
matrix_B = matrix_B.drop(['index'], axis = 1)




fruits_of_labor_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('fruits of labor')]
# Zijn er geen een van





receivings_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('receivings')]
# Zijn er geen een van





merits_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('merits')]
merits_matrix = merits_matrix.reset_index()
merits_matrix = merits_matrix[~merits_matrix.path_data.isin(matrix_B.path_data)]
merits_matrix = merits_matrix.reset_index()
merits_matrix = merits_matrix.drop(['index'], axis = 1)

merits_matrix['Name'] = 1

merits_matrix['Phone_number'] = 0
merits_matrix.loc[[0,3,7,8], 'Phone_number'] = 1

merits_matrix['Fax'] = 0
merits_matrix.loc[[0,3,7], 'Fax'] = 1

merits_matrix['Email'] = 0
merits_matrix.loc[[3,7,8], 'Email'] = 1

merits_matrix['Job_title'] = 0
merits_matrix.loc[[7], 'Job_title'] = 1

# merge met matrix_B
matrix_B = pd.concat([matrix_B, merits_matrix])
matrix_B = matrix_B.reset_index()
matrix_B = matrix_B.drop(['index'], axis = 1)



renumeration_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('remuneration')]
renumeration_matrix = renumeration_matrix.reset_index()
renumeration_matrix = renumeration_matrix[~renumeration_matrix.path_data.isin(matrix_B.path_data)]
renumeration_matrix = renumeration_matrix.reset_index()
renumeration_matrix = renumeration_matrix.drop(['index'], axis = 1)
# Blijft niks over




earnings_matrix = body_matrix_B[body_matrix_B.bodies.str.contains('earnings')]
earnings_matrix = earnings_matrix.reset_index()
earnings_matrix = earnings_matrix[~earnings_matrix.path_data.isin(matrix_B.path_data)]
earnings_matrix = earnings_matrix.reset_index()
earnings_matrix = earnings_matrix.drop(['index'], axis = 1)

earnings_matrix['Name'] = 1
earnings_matrix.loc[[11,15,16,22,24,25,26,27,37,47,48,49,50,51,62,66,67,68,74], 'Name'] = 0

earnings_matrix['Email'] = 0
earnings_matrix.loc[[0,2,3,5,6,8,9,10,28,34,36,38,54,55,59,60,61,70,72], 'Email'] = 1

earnings_matrix['Job_title'] = 0
earnings_matrix.loc[[0,2,5,6,8,9,10,13,18,34,38,39,42,54,55,59,60,61,63], 'Job_title'] = 1

earnings_matrix['Phone_number'] = 0
earnings_matrix.loc[[54,55,60,63,72], 'Phone_number'] = 1

#merge with matrix_B
matrix_B = pd.concat([matrix_B, earnings_matrix])
matrix_B = matrix_B.reset_index()
matrix_B = matrix_B.drop(['index'], axis = 1)


matrix_B.to_hdf('matrix_B.hdf', 'mydata', mode = 'w')





