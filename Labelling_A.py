#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 11:43:28 2018

@author: ilsegoedhart
"""

import os
import string
import nltk
import pandas as pd
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import TfidfVectorizer

# Trying to get the matrix into a pd dataframe
# import dask.dataframe as dd
# from scipy.sparse import csr_matrix

## Print pathnames and safe
#rootdir = "../maildir/"
#path_data = []
#
#for directory, subdirectory, files in os.walk(rootdir):
#    for name in files:
#        path = os.path.join(directory, name)
#        path_data.append(path)
#
## Delete paths with DS_Store, because it does not exist
#path_matrix = pd.DataFrame({'path_data': path_data})
#path_matrix = path_matrix[~path_matrix.path_data.str.contains('DS_Store')]
#
## Keep only emails from the sent folder, to prevent double email context in matrix
#path_matrix = path_matrix[path_matrix.path_data.str.contains('/sent/')]
#
## Keep only first 30000 emails
#path_matrix = path_matrix.head(30000)
#
## Change pd to list
#path_data = path_matrix['path_data'].tolist()
#
## Empty list for all email bodies
#email_bodies = []
#
#for path in path_data:
#    print(path)
#    email = open(path, "r").read()
#    
#    # Delete all information as; from, to, subject, date, time, etc.
#    content_email = email.split("X-FileName:")
#    email_body = ""
#    if len(content_email) > 1:
#        
#        # Keep body of email
#        text_string = content_email[1]
#        
#        email_bodies.append(text_string)
#
## Search in pd dataframe for: salary, bonus
#body_matrix = pd.DataFrame(email_bodies)
#body_matrix["path_data"] = path_data
#body_matrix = body_matrix.set_index("path_data")
#body_matrix = body_matrix.rename(columns={0: 'bodies'})
#
#body_matrix.to_hdf('bodymatrix.hdf', 'mydata', mode = 'w')

body_matrix = pd.read_hdf('bodymatrix.hdf' , 'mydata')

salary_matrix = body_matrix[body_matrix.bodies.str.contains('salary')]
salary_matrix = salary_matrix.reset_index()
salary_matrix['Name'] = 0
salary_matrix.loc[[0,1,2,3], 'Name'] = 1
salary_matrix.loc[9:22, 'Name'] = 1
salary_matrix.loc[24:68, 'Name'] = 1

salary_matrix['Email'] = 0
salary_matrix.loc[[0,2,6,20,22,29,30,33,35,37,39,42,44,46,49,51,53,55,62,63,], 'Email'] = 1

salary_matrix['Company'] = 0
salary_matrix.loc[[0,2,13], 'Company'] = 1

salary_matrix['Age'] = 0
salary_matrix.loc[[0,2,56,61,62], 'Age'] = 1

salary_matrix['Education'] = 0
salary_matrix.loc[[0,62], 'Education'] = 1

salary_matrix['Job_title'] = 0
salary_matrix.loc[[0,2,3,10,16,21,22,24,29,31,48,56,59,61,62,67], 'Job_title'] = 1

salary_matrix['Salary'] = 0
salary_matrix.loc[[2,3,11,13,14,15,17,18,22,26,36,56,61,68], 'Salary'] = 1

salary_matrix['Bonus'] = 0
salary_matrix.loc[[2,10,11,14,15,18,22,26,50], 'Bonus'] = 1

salary_matrix['Phone_number'] = 0
salary_matrix.loc[[6,20,22,49,53,57,61,62,63,68], 'Phone_number'] = 1

salary_matrix['Fax'] = 0
salary_matrix.loc[[6,22,61,62], 'Fax'] = 1




bonus_matrix = body_matrix[body_matrix.bodies.str.contains('bonus')]
bonus_matrix = bonus_matrix.reset_index()
bonus_matrix['Name'] = 1
bonus_matrix.loc[[0,3,5,11,12,14,31,33,34,39,43,47,51,56,57,62,64,79,86,88,90,93,95,102,109,118,121,], 'Name'] = 0

bonus_matrix['Email'] = 0
bonus_matrix.loc[[3,16,17,20,24,32,37,45,55,61,68,72,73,80,81,83,89,90,91,101,103,118,124], 'Email'] = 1

bonus_matrix['Job_title'] = 0
bonus_matrix.loc[[6,7,13,15,22,29,32,37,41,42,81,83,89,91,94,96,97,105,124], 'Job_title'] = 1

bonus_matrix['Phone_number'] = 0
bonus_matrix.loc[[16,17,32,37,40,41,42,58,60,61,67,73,96,124], 'Phone_number'] = 1

bonus_matrix['Fax'] = 0
bonus_matrix.loc[[16,17,32,37,103,124], 'Fax'] = 1

bonus_matrix['Bonus'] = 0
bonus_matrix.loc[[29,32,37,54,82,92,94,120], 'Bonus'] = 1



# Vergelijk de twee matrixen op dubbelen en dan de dubbelen verwijderen
matrix_A = pd.concat([bonus_matrix, salary_matrix])
matrix_A = matrix_A.reset_index()
matrix_A.loc[[2,4], 'Job_title'] = 1
matrix_A.loc[[2,4], 'Email'] = 1
matrix_A.loc[[2], 'Education'] = 1
matrix_A.loc[[2, 4], 'Company'] = 1
matrix_A.loc[[2, 4], 'Age'] = 1
matrix_A.loc[[4,21,25,27,30,32,35], 'Salary'] = 1
matrix_A.loc[[4,21,25,27,35], 'Bonus'] = 1
matrix_A = matrix_A.drop_duplicates('path_data')




# Synonyms salary
salaries_matrix = body_matrix[body_matrix.bodies.str.contains('salaries')]

# Eerst checken welke niet al in salary_matrix staan, en dan overige labellen
salaries_matrix = salaries_matrix.reset_index()
salaries_matrix = salaries_matrix[~salaries_matrix.path_data.isin(matrix_A.path_data)]

salaries_matrix['Name'] = 1
salaries_matrix.loc[[3], 'Name'] = 0

salaries_matrix['Job_title'] = 0
salaries_matrix.loc[[7], 'Job_title'] = 1

salaries_matrix['Phone_number'] = 0
salaries_matrix.loc[[7, 11], 'Phone_number'] = 1

salaries_matrix['Fax'] = 0
salaries_matrix.loc[[7], 'Fax'] = 1

salaries_matrix['Salary'] = 0
salaries_matrix.loc[[11], 'Salary'] = 1

salaries_matrix['Email'] = 0
salaries_matrix.loc[[11], 'Email'] = 1

# Mergen met matrix_A
matrix_A = pd.concat([matrix_A, salaries_matrix])
matrix_A = matrix_A.reset_index()

wage_matrix = body_matrix[body_matrix.bodies.str.contains('wage')]
wage_matrix = wage_matrix.reset_index()
wage_matrix = wage_matrix[~wage_matrix.path_data.isin(matrix_A.path_data)]

wage_matrix['Name'] = 1
wage_matrix.loc[[21], 'Name'] = 0

wage_matrix['Phone_number'] = 1
wage_matrix.loc[[22,21,25,12,16,17,15,9,7,13,26,20,19], 'Phone_number'] = 0

wage_matrix['Email'] = 0
wage_matrix.loc[[3,23,11,10,17,7,13,8,14,20], 'Email'] = 1

wage_matrix['Job_title'] = 0
wage_matrix.loc[[3,24,18,14], 'Job_title'] = 1

wage_matrix['Fax'] = 0
wage_matrix.loc[[23,11,10,8,14,19], 'Fax'] = 1

wage_matrix['Age'] = 0
wage_matrix.loc[[7], 'Age'] = 1

wage_matrix['Bonus'] = 0
wage_matrix.loc[[26], 'Bonus'] = 1

# Merge wage_matrix met matrix_A
matrix_A = pd.concat([matrix_A, wage_matrix])
matrix_A = matrix_A.drop(['index', 'level_0'], axis = 1)
matrix_A = matrix_A.reset_index()
matrix_A = matrix_A.drop(['index'], axis = 1)





pay_matrix = body_matrix[body_matrix.bodies.str.contains('pay')]
# zijn er wel heel veel dus doe ik later




income_matrix = body_matrix[body_matrix.bodies.str.contains('income')]
income_matrix = income_matrix.reset_index()
income_matrix = income_matrix[~income_matrix.path_data.isin(matrix_A.path_data)]
income_matrix = income_matrix.reset_index()
income_matrix = income_matrix.drop(['index'], axis =1)

income_matrix['Name'] = 1
income_matrix.loc[[2,4,9,11,33,37,44,49,50,55,67,75,86,87,75,86,87,90,95,101,106,112,125,131,132,133,134,150,154,160], 'Name'] = 0

income_matrix['Email'] = 0
income_matrix.loc[[1,3,14,15,16,17,18,19,20,22,23,24,27,28,31,35,42,53,57,58,59,61,62,63,64,65,68,71,76,81,83,84,91,95,98,100,103,104,107,108,109,113,114,115,116,117,118,120,121,130,138,139,155,157], 'Email'] = 1

income_matrix['Phone_number'] = 0
income_matrix.loc[[3,7,14,15,16,19,20,22,24,27,31,46,57,58,59,60,61,62,63,64,65,68,69,70,71,81,83,84,96,98,100,103,104,107,108,110,113,120,129,155,157], 'Phone_number'] = 1

income_matrix['Job_title'] = 0
income_matrix.loc[[3,10,15,16,17,18,19,22,23,27,32,41,42,48,54,57,74,84,88,89,93,96,100,103,107,108,110,115,120,138], 'Job_title'] = 1

income_matrix['Fax'] = 0
income_matrix.loc[[19,22,27,42,63,64,65,68,81,84,100,103,104,107,110,120], 'Fax'] = 1

income_matrix['Salary'] = 0
income_matrix.loc[[66], 'Salary'] = 1

income_matrix['Age'] = 0
income_matrix.loc[[74,114,115], 'Age'] = 1

# Merge income matrix met matrix_A
matrix_A = pd.concat([matrix_A, income_matrix])
matrix_A = matrix_A.reset_index()
matrix_A = matrix_A.drop(['index'], axis =1)






payment_matrix = body_matrix[body_matrix.bodies.str.contains('payment')]
# zijn er wel heel veel dus doe ik later






allowance_matrix = body_matrix[body_matrix.bodies.str.contains('allowance')]
allowance_matrix = allowance_matrix.reset_index()
allowance_matrix = allowance_matrix[~allowance_matrix.path_data.isin(matrix_A.path_data)]
allowance_matrix = allowance_matrix.reset_index()
allowance_matrix = allowance_matrix.drop(['index'], axis =1)

allowance_matrix['Name'] = 1
allowance_matrix.loc[[3,11], 'Name'] = 0

allowance_matrix['Email'] = 0
allowance_matrix.loc[[0,1,2,4,5,6,8,10,12,13,14], 'Email'] = 1

allowance_matrix['Job_title'] = 0
allowance_matrix.loc[[0,2,4,9,12,14,16], 'Job_title'] = 1

allowance_matrix['Phone_number'] = 0
allowance_matrix.loc[[2,4,5,6,8,10,12,16], 'Phone_number'] = 1

allowance_matrix['Fax'] = 0
allowance_matrix.loc[[8,12], 'Fax'] = 1

# Merge allowance_matrix met matrix_A
matrix_A = pd.concat([matrix_A, allowance_matrix])
matrix_A = matrix_A.reset_index()
matrix_A = matrix_A.drop(['index'], axis = 1)





fee_matrix = body_matrix[body_matrix.bodies.str.contains('fee')]
# zijn er wel heel veel dus doe ik later





wages_matrix = body_matrix[body_matrix.bodies.str.contains('wages')]
wages_matrix = wages_matrix.reset_index()
wages_matrix = wages_matrix[~wages_matrix.path_data.isin(matrix_A.path_data)]
# allemaal staan al in matrix_A




compensation_matrix = body_matrix[body_matrix.bodies.str.contains('compensation')]
compensation_matrix = compensation_matrix.reset_index()
compensation_matrix = compensation_matrix[~compensation_matrix.path_data.isin(matrix_A.path_data)]
compensation_matrix = compensation_matrix.reset_index()
compensation_matrix = compensation_matrix.drop(['index'], axis = 1)

compensation_matrix['Name'] = 1
compensation_matrix.loc[[2,11,26,27,45,53,59], 'Name'] = 0

compensation_matrix['Email'] = 0
compensation_matrix.loc[[0,3,4,5,8,9,12,14,16,30,31,33,36,37,38,39,44,48,52,54,55,56,57,60,61,62,63,65,66], 'Email'] = 1

compensation_matrix['Phone_number'] = 0
compensation_matrix.loc[[0,3,4,5,7,9,14,16,30,31,33,36,37,39,44,48,52,54,55], 'Phone_number'] = 1

compensation_matrix['Fax'] = 0
compensation_matrix.loc[[0,33,36,37,54,55], 'Fax'] = 1

compensation_matrix['Education'] = 0
compensation_matrix.loc[[4], 'Education'] = 1

compensation_matrix['Job_title'] = 0
compensation_matrix.loc[[5,7,8,9,12,14,16,18,19,22,33,35,36,39,41,42,43,44,46,48,54,60], 'Job_title'] = 1

# Merge compensation_matrix to matrix_A
matrix_A = pd.concat([matrix_A, compensation_matrix])
matrix_A = matrix_A.reset_index()
matrix_A = matrix_A.drop(['index'], axis = 1)



fruits_of_labor_matrix = body_matrix[body_matrix.bodies.str.contains('fruits of labor')]
# zijn geen emails met dit woord



receivings_matrix = body_matrix[body_matrix.bodies.str.contains('receivings')]
# zijn geen mails met dit woord




merits_matrix = body_matrix[body_matrix.bodies.str.contains('merits')]
merits_matrix = merits_matrix.reset_index()
merits_matrix = merits_matrix[~merits_matrix.path_data.isin(matrix_A.path_data)]
merits_matrix = merits_matrix.reset_index()
merits_matrix = merits_matrix.drop(['index'], axis = 1)

merits_matrix['Name'] = 1
merits_matrix.loc[[8,9,13,21], 'Name'] = 0

merits_matrix['Email'] = 0
merits_matrix.loc[[0,2,4,5,6,7,10,11,12,14,15,16], 'Email'] = 1

merits_matrix['Job_title'] = 0
merits_matrix.loc[[1,5,18], 'Job_title'] = 1

merits_matrix['Phone_number'] = 0
merits_matrix.loc[[5], 'Phone_number'] = 1

merits_matrix['Fax'] = 0
merits_matrix.loc[[5], 'Fax'] = 1


# merge merits_matrix to matrix_A
matrix_A = pd.concat([matrix_A, merits_matrix])
matrix_A = matrix_A.reset_index()
matrix_A = matrix_A.drop(['index'], axis = 1)


renumeration_matrix = body_matrix[body_matrix.bodies.str.contains('remuneration')]
# zijn geen emails met dit woord


earnings_matrix = body_matrix[body_matrix.bodies.str.contains('earnings')]
earnings_matrix = earnings_matrix.reset_index()
earnings_matrix = earnings_matrix[~earnings_matrix.path_data.isin(matrix_A.path_data)]
earnings_matrix = earnings_matrix.reset_index()
earnings_matrix = earnings_matrix.drop(['index'], axis =1)

earnings_matrix['Name'] = 1
earnings_matrix.loc[[13,15,17,19,21,23,24,26,32,34,39,50,58,59,60,61,70,89,110], 'Name'] = 0

earnings_matrix['Phone_number'] = 0
earnings_matrix.loc[[0,9,16,20,54,55,56,57,62,63,69,82,86,87,88,95,100,103,120,122,125], 'Phone_number'] = 1

earnings_matrix['Email'] = 0
earnings_matrix.loc[[1,2,5,11,20,50,54,56,57,58,62,63,66,67,74,75,76,82,86,87,95,96,100,111,117], 'Email'] = 1

earnings_matrix['Job_title'] = 0
earnings_matrix.loc[[3,5,7,8,9,10,12,16,20,31,33,38,40,47,62,63,64,68,73,78,82,83,85,87,88,119], 'Job_title'] = 1

earnings_matrix['Fax'] = 0
earnings_matrix.loc[[9,63,82,87], 'Fax'] = 1


# Merge earnings_matrix met matrix_A
matrix_A = pd.concat([matrix_A, earnings_matrix])
matrix_A = matrix_A.reset_index()
matrix_A = matrix_A.drop(['index'], axis = 1)



# Synonyms for bonus
# Ga ik niet doen

matrix_A.to_hdf('matrix_A.hdf', 'mydata', mode = 'w')




