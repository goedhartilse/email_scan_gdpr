#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  8 20:08:55 2018

@author: ilsegoedhart
"""

# Good code

import os
import string
import nltk
import pandas as pd
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import TfidfVectorizer

# Trying to get the matrix into a pd dataframe
# import dask.dataframe as dd
# from scipy.sparse import csr_matrix

## Print pathnames and safe
#rootdir = "../maildir/"
#path_data = []
#
#for directory, subdirectory, files in os.walk(rootdir):
#    for name in files:
#        path = os.path.join(directory, name)
#        path_data.append(path)
#
## Delete paths with DS_Store, because it does not exist
#path_matrix = pd.DataFrame({'path_data': path_data})
#path_matrix = path_matrix[~path_matrix.path_data.str.contains('DS_Store')]
#
## Keep only emails from the sent folder, to prevent double email context in matrix
#path_matrix = path_matrix[path_matrix.path_data.str.contains('/sent/')]
#
## Keep only first 40000 emails
#path_matrix = path_matrix.head(30000)
#
## Change pd to list
#path_data = path_matrix['path_data'].tolist()
#
## Empty list for all email bodies
#email_bodies = []
#
#for path in path_data:
#    print(path)
#    email = open(path, "r").read()
#    
#    # Delete all information as; from, to, subject, date, time, etc.
#    content_email = email.split("X-FileName:")
#    email_body = ""
#    if len(content_email) > 1:
#        
#        # Remove punctuations
#        text_string = content_email[1].translate(str.maketrans("", "", string.punctuation))
#        
#        # Split up sesntence into single words
#        single_words = nltk.word_tokenize(text_string)
#        
#        # Create English stemmer
#        stemmer = SnowballStemmer("english")
#        
#        # Empty list for all stems
#        stems = []
#        
#        # Extract stem for each word
#        for word in single_words:
#            stem = stemmer.stem(word)
#            stems.append(stem)
#        
#        # Attach all words into a string which is the body of the email
#        email_body = ' '.join(stem for stem in stems)
#        
#        # Fill empty email_bodies list with all email bodies
#        email_bodies.append(email_body)
#
## Create tf idf values and remove English stopwords
#vectorizer = TfidfVectorizer(stop_words = "english", lowercase=True)
#matrix = vectorizer.fit_transform(email_bodies)
#
## Return all words of matrix
#vocab_list = vectorizer.get_feature_names()
#
## Create panda datframe
#matrixdf = pd.DataFrame(matrix.toarray())
#matrixdf["path_data"] = path_data
#matrixdf = matrixdf.set_index("path_data")
#matrixdf.columns = vocab_list
#matrixdf.info()
#
#matrixdf.to_hdf('tdidfmatrix.hdf', 'mydata', mode='w')

tdidfmatrix = pd.read_hdf('tdidfmatrix.hdf', 'mydata')

